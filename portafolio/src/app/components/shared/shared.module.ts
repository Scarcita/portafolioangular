import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//angular material
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';




@NgModule({
  declarations: [],
  
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule
    
  ],

  exports: [
    MatToolbarModule,
    MatIconModule
  ]
  
})

export class SharedModule { }



