import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ContactosComponent } from './contactos/contactos.component';

const routes: Routes = [
  {path: '', component: DashboardComponent,
children:
  [  
    {path: '', component: InicioComponent},
    {path: '', component: AgendaComponent},
    { path: '', component: ContactosComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
